# README #

In order to run minesweeper you need to install PyGame
http://pygame.org/download.shtml

Easy way is to install it via wheel, see e.g. http://stackoverflow.com/a/28127864
If 64-bit wheel (.whl) package installation fails, try its 32-bit version.

You can also find .msi installation package in their bitbucket repo
https://bitbucket.org/pygame/pygame/downloads 

Contact me at
herman.yanush3@gmail.com